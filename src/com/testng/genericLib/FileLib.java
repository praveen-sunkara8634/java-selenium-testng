package com.testng.genericLib;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * 
 * @author PRAVEEN SUNKARA
 *
 */

public class FileLib {
	
	/**
	 * 
	 * @param key
	 * @return value attached to the key argument
	 * @throws Throwable
	 */
	public String getKeyValueProperty(String key) throws Throwable
	{
		FileInputStream fis = new FileInputStream("./Data/commonData.properties");
		Properties pObj = new Properties();
		pObj.load(fis);
		String val = pObj.getProperty(key);
		return val;
	}
	
	/**
	 * 
	 * @param sheetName
	 * @param row
	 * @param cell
	 * @return data in the cell 
	 * @throws Throwable
	 */
	
	public String getExcelData(String sheetName, int row, int cell) throws Throwable
	{
		FileInputStream fis = new FileInputStream("./Data/testData.xlsx");
		Workbook wb = WorkbookFactory.create(fis);
		
		Sheet sh = wb.getSheet(sheetName);
		Row rowNo = sh.getRow(row);
		Cell cellNo = rowNo.getCell(cell);
		String data = cellNo.getStringCellValue();
		return data;
	}
	
	/**
	 * 
	 * @param data
	 * @param sheetName
	 * @param row
	 * @param cell
	 * @throws Throwable
	 */
	
	public void setExcelData(String data, String sheetName, int row, int cell) throws Throwable
	{
		FileInputStream fis = new FileInputStream("./Data/testData.xlsx");
		Workbook wb = WorkbookFactory.create(fis);
		Sheet sh = wb.getSheet(sheetName);
		Row rowNo = sh.getRow(row);
		Cell cellNo = rowNo.getCell(cell);
		cellNo.setCellValue(data);
		
		FileOutputStream fos = new FileOutputStream("./Data/testData.xlsx");
		wb.write(fos);
		wb.close();
	}
}
