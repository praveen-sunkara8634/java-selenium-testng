package com.testng.genericLib;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import com.testng.objectreporsitorylib.Home;
import com.testng.objectreporsitorylib.Login;

public class BaseClass {
	public static WebDriver driver;
	public FileLib flib = new FileLib();

	@BeforeSuite
	public void configBS() {
		System.out.println("connect to data base");
	}

	@Parameters("browser")
	@BeforeTest
	public void configBT(String browserName) {
		if (browserName.equals("firefox")) {
			driver = new FirefoxDriver();
		} else if (browserName.equals("chrome")) {
			driver = new ChromeDriver();
		}
	}

	@BeforeClass
	public void configBC() throws Throwable {
		System.out.println("==============Launch browser============");
		String browser = flib.getKeyValueProperty("browser");
		if (browser.equals("firefox")) {
			driver = new FirefoxDriver();
		} else if (browser.equals("chrome")) {
			driver = new ChromeDriver();
		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@BeforeMethod
	public void configBM() throws Throwable {
		String URL = flib.getKeyValueProperty("url");
		driver.get(URL);
		System.out.println("login");
		/*
		 * driver.findElement(By.name("username")).sendKeys(flib.getKeyValueProperty(
		 * "./commonData.properties", "user"));
		 * driver.findElement(By.name("password")).sendKeys(flib.getKeyValueProperty(
		 * "./commonData.properties", "pass"));
		 * driver.findElement(By.id("loginButton")).click();
		 */
		Login lp = PageFactory.initElements(driver, Login.class);
		lp.loginToApp();
	}

	@AfterMethod
	public void configAM() {
		System.out.println("logout");
		/* driver.findElement(By.id("logoutLink")).click(); */
		Home hp = PageFactory.initElements(driver, Home.class);
		hp.logout();
	}

	@AfterClass
	public void configAC() {
		System.out.println("=============CLOSE BROWSER=======================");
		driver.close();
	}

}
