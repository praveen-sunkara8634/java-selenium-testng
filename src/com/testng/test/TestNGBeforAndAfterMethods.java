package com.testng.test;

import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestNGBeforAndAfterMethods {
	
	@BeforeMethod
	public void configBM() {
		System.out.println("before mtd");
	}
	
	@Test
	public void createCustomer()
	{
		System.out.println("execute create");
	}
	
	@Test
	public void modifyCustomer()
	{
		System.out.println("execute modify");
	}
	
	@AfterMethod
	public void configAM() {
		System.out.println("after mtd");
	}

}
