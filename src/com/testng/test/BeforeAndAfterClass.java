package com.testng.test;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class BeforeAndAfterClass {
	
	@BeforeClass
	public void configBC() {
		System.out.println("==========START================");
	}
	
	@BeforeMethod
	public void configBM() {
		System.out.println("login");
	}
	
	@Test
	public void createCustomer()
	{
		System.out.println("execute create");
	}
	
	@Test
	public void modifyCustomer()
	{
		System.out.println("execute modify");
	}
	
	@AfterMethod
	public void configAM() {
		System.out.println("logout");
	}
	
	@AfterClass
	public void configAC() {
		System.out.println("===================END==================");
	}
}
