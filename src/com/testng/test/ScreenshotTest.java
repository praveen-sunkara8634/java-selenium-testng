package com.testng.test;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class ScreenshotTest {
	public static void main(String[] args) throws Throwable {
		WebDriver driver = new ChromeDriver();
		driver.get("https://amazon.com");

		// capture the file type of screenshot using "EventFiringWebDriver" class
		EventFiringWebDriver eDriver = new EventFiringWebDriver(driver);
		File srcFile = eDriver.getScreenshotAs(OutputType.FILE);

		// copy the screenshot to destination location
		// create a new file in destination location
		File dstFile = new File("F://JAVA//file/img.jpg");
		FileUtils.copyFile(srcFile, dstFile);

	}

}
