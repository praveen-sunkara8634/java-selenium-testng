package com.testng.test;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.testng.genericLib.BaseClass;


@Listeners(com.testng.genericLib.ListenerImpClass.class)
public class HomePageTest extends BaseClass {
	@Test(priority=1)
	public void HomePageVerifyTest()
	{
		/*Verify the home page*/
		String expTitle = "acttime - task";
		/*capture the home page title from GUI*/
		String acttitle= driver.getTitle();
		Assert.assertEquals(acttitle, expTitle);
		
	}
		
		@Test(priority=2)
		public void homePageLogoImageVerifyTest()
		{
			/*identify & verify Logo image*/
			boolean status = driver.findElement(By.xpath("//img[contains(@src,'default-logo.png')]")).isEnabled();
			Assert.assertTrue(status);
		}
	}
	
	

