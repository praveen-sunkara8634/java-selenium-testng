package com.testng.datadrivenframeworkexamples;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class AddToCart {
	@Test(dataProvider="getDataForAddToCartTest")
	public void addToCart(String productName, String prodQty)
	{
		System.out.println("by product==>"+productName+"==>Qty==>"+prodQty);
	}
	
	@DataProvider
	public Object[][] getDataForAddToCartTest()
	{
		Object[][] objArr = new Object[4][2];
		objArr[0][0] = "iPhone 10x";
		objArr[0][1] = "1";
		
		objArr[1][0] = "iPhone 7";
		objArr[1][1] = "10";
		
		objArr[2][0] = "Samsung S9";
		objArr[2][1] = "5";
		
		objArr[3][0] = "iPhone 4S";
		objArr[3][1] = "1";

		return objArr;
	}

}
