package com.testng.customertest;

import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.testng.genericLib.BaseClass;
import com.testng.objectreporsitorylib.CreateCustomer;
import com.testng.objectreporsitorylib.Home;
import com.testng.objectreporsitorylib.OpenTask;
import com.testng.objectreporsitorylib.ProjectAndCustomers;

public class CustomerTest extends BaseClass {
	@Test
	public void createCustomerTest() throws Throwable
	{
		/*read data from excel*/
		String customerName = flib.getExcelData("Sheet1", 1, 2);
		/*navigate to task page*/
		Home hp = PageFactory.initElements(driver, Home.class);
		hp.clickOnTaskImage();
		/*navigate to Project and Customer Page*/
		OpenTask op = PageFactory.initElements(driver, OpenTask.class);
		op.clickOnProjAndCustLnk();
		/*navigate to create customer page*/
		ProjectAndCustomers pac = PageFactory.initElements(driver, ProjectAndCustomers.class);
		pac.clickOnCreateCustomerBtn();
		
		/*create customer with name*/
		CreateCustomer cp = PageFactory.initElements(driver, CreateCustomer.class);
		cp.createCustomer(customerName);
		/*verify*/
		String actSussMsg = pac.getSuccessmsg().getText();
		boolean status = actSussMsg.contains("successfully created");
		Assert.assertTrue(status);
	}
		
		@Test
		public void createCustomerWithDescriptionTest() throws Throwable
		{
			/*read data from excel*/
			String customerName = flib.getExcelData("Sheet1", 4, 2);
			String customerDesc = flib.getExcelData("Sheet1", 4, 3);
			/*navigate to task page*/
			Home hp = PageFactory.initElements(driver, Home.class);
			hp.clickOnTaskImage();
			/*navigate to Project and Customer Page*/
			OpenTask op = PageFactory.initElements(driver, OpenTask.class);
			op.clickOnProjAndCustLnk();
			/*navigate to create customer page*/
			ProjectAndCustomers pac = PageFactory.initElements(driver, ProjectAndCustomers.class);
			pac.clickOnCreateCustomerBtn();
			
			/*create customer with name*/
			CreateCustomer cp = PageFactory.initElements(driver, CreateCustomer.class);
			cp.createCustomer(customerName);
			/*verify*/
			String actSussMsg = pac.getSuccessmsg().getText();
			boolean status = actSussMsg.contains("successfully created");
			Assert.assertTrue(status);
		
	}

}
