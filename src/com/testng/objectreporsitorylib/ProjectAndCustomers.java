package com.testng.objectreporsitorylib;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProjectAndCustomers {
	@FindBy(xpath="//span[text(),'Create Customer']")
	private WebElement createCustomerBtn;
	
	@FindBy(xpath="//span[@class='successmsh']")
	private WebElement successmsg;
	
	public void clickOnCreateCustomerBtn() {
		createCustomerBtn.click();
	}

	public WebElement getSuccessmsg() {
		return successmsg;
	}

}
