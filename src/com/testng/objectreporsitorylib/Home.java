package com.testng.objectreporsitorylib;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

public class Home {
	@FindBy(xpath="//div[text(),'TASKS']")
	private WebElement taskImg;
	
	@FindBy(xpath="//div[text(),'USER']")
	private WebElement userImg;
	
	@FindBy(xpath="//div[text(),'Logout']")
	private WebElement logoutLink;
	
	@FindBys({@FindBy(xpath="//div[text(),'Logout']")})
	private WebElement kk;
	
	public void clickOnTaskImage()
	{
		taskImg.click();
	}
	public void clickOnUserImage()
	{
		userImg.click();
	}
	public void logout()
	{
		logoutLink.click();
	}

}
