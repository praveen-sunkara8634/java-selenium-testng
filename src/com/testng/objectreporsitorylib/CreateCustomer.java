package com.testng.objectreporsitorylib;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateCustomer {
	@FindBy(id="customerLighBox_nameField")
	private WebElement customerName;
	
	@FindBy(id="customerLighBox_descriptionField")
	private WebElement customerDesc;
	
	@FindBy(xpath="//span[@class='buttonTitle' and text()='Create Customer']")
	private WebElement createCustomerBtn;
	
	public void createCustomer(String customerName) {
		this.customerName.sendKeys(customerName);
		createCustomerBtn.click();
	}
	
	public void createCustomer(String customerName, String customerDesc) {
		this.customerName.sendKeys(customerName);
		this.customerDesc.sendKeys(customerDesc);
		createCustomerBtn.click();
	}

}
