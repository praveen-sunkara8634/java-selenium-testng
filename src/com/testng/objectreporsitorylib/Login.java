package com.testng.objectreporsitorylib;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.testng.genericLib.FileLib;

public class Login {

	@FindBy(name = "username")
	private WebElement userEdt;
	@FindBy(name = "pwd")
	private WebElement passEdt;
	@FindBy(id = "loginButton")
	private WebElement loginBtn;

	public void loginToApp() throws Throwable {
		FileLib fLib = new FileLib();

		userEdt.sendKeys(fLib.getKeyValueProperty("username"));
		passEdt.sendKeys(fLib.getKeyValueProperty("password"));
		loginBtn.click();
	}

	public void loginToApp(String username, String password) throws Throwable {
		userEdt.sendKeys(username);
		passEdt.sendKeys(password);
		loginBtn.click();
	}

	public WebElement getUserEdt() {
		return userEdt;
	}

	public WebElement getPassEdt() {
		return passEdt;
	}

	public WebElement getLoginBtn() {
		return loginBtn;
	}
}
